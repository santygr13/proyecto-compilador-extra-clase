﻿namespace CompiladorExtraClaseEquipo
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtNombreArchivo = new System.Windows.Forms.TextBox();
            this.btnCargarArchivo = new System.Windows.Forms.Button();
            this.txtTextoConsola = new System.Windows.Forms.TextBox();
            this.txtTextoArchivo = new System.Windows.Forms.TextBox();
            this.btnLeer = new System.Windows.Forms.Button();
            this.btnLimpiar = new System.Windows.Forms.Button();
            this.btnCompilar = new System.Windows.Forms.Button();
            this.tablaTablas = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.dataTablaSimbolos = new System.Windows.Forms.DataGridView();
            this.categoria = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lexema = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nroLinea = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.posInicial = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.posFinal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.dataTablaError = new System.Windows.Forms.DataGridView();
            this.lexemaError = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nroLineaError = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.posInicialError = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.posFinalError = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.falla = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.causa = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.solucion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.dataTablaDummy = new System.Windows.Forms.DataGridView();
            this.categoriaDummy = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lexemaDummy = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nroLineaDummy = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.posInicialDummy = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.posFinalDummy = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dummy = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.dataTablaLiterales = new System.Windows.Forms.DataGridView();
            this.categoriaLiteral = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lexemaLiteral = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nroLineaLiteral = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.posInicialLietral = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.posFinalLiteral = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.dataPalabrasReservadas = new System.Windows.Forms.DataGridView();
            this.categoriaReservadas = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lexemaReservadas = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nroLineaReservadas = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.posInicialReservadas = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.posFinalReservadas = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnTablaSimbolos = new System.Windows.Forms.Button();
            this.btnTablaErrores = new System.Windows.Forms.Button();
            this.btnTablaDummy = new System.Windows.Forms.Button();
            this.btnTablaLiterales = new System.Windows.Forms.Button();
            this.btnPalabrasReservadas = new System.Windows.Forms.Button();
            this.temporal = new System.Windows.Forms.TextBox();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.tablaTablas.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataTablaSimbolos)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataTablaError)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataTablaDummy)).BeginInit();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataTablaLiterales)).BeginInit();
            this.tabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataPalabrasReservadas)).BeginInit();
            this.SuspendLayout();
            // 
            // txtNombreArchivo
            // 
            this.txtNombreArchivo.Location = new System.Drawing.Point(74, 23);
            this.txtNombreArchivo.Name = "txtNombreArchivo";
            this.txtNombreArchivo.Size = new System.Drawing.Size(356, 23);
            this.txtNombreArchivo.TabIndex = 0;
            // 
            // btnCargarArchivo
            // 
            this.btnCargarArchivo.Location = new System.Drawing.Point(436, 23);
            this.btnCargarArchivo.Name = "btnCargarArchivo";
            this.btnCargarArchivo.Size = new System.Drawing.Size(121, 23);
            this.btnCargarArchivo.TabIndex = 1;
            this.btnCargarArchivo.Text = "Cargar Archivo";
            this.btnCargarArchivo.UseVisualStyleBackColor = true;
            this.btnCargarArchivo.Click += new System.EventHandler(this.btnCargarArchivo_Click);
            // 
            // txtTextoConsola
            // 
            this.txtTextoConsola.Location = new System.Drawing.Point(12, 75);
            this.txtTextoConsola.Multiline = true;
            this.txtTextoConsola.Name = "txtTextoConsola";
            this.txtTextoConsola.Size = new System.Drawing.Size(263, 322);
            this.txtTextoConsola.TabIndex = 2;
            // 
            // txtTextoArchivo
            // 
            this.txtTextoArchivo.Location = new System.Drawing.Point(294, 75);
            this.txtTextoArchivo.Multiline = true;
            this.txtTextoArchivo.Name = "txtTextoArchivo";
            this.txtTextoArchivo.Size = new System.Drawing.Size(263, 322);
            this.txtTextoArchivo.TabIndex = 2;
            // 
            // btnLeer
            // 
            this.btnLeer.Location = new System.Drawing.Point(12, 415);
            this.btnLeer.Name = "btnLeer";
            this.btnLeer.Size = new System.Drawing.Size(75, 23);
            this.btnLeer.TabIndex = 3;
            this.btnLeer.Text = "Leer Texto";
            this.btnLeer.UseVisualStyleBackColor = true;
            this.btnLeer.Click += new System.EventHandler(this.btnLeer_Click);
            // 
            // btnLimpiar
            // 
            this.btnLimpiar.Location = new System.Drawing.Point(93, 415);
            this.btnLimpiar.Name = "btnLimpiar";
            this.btnLimpiar.Size = new System.Drawing.Size(75, 23);
            this.btnLimpiar.TabIndex = 4;
            this.btnLimpiar.Text = "Limpiar";
            this.btnLimpiar.UseVisualStyleBackColor = true;
            this.btnLimpiar.Click += new System.EventHandler(this.btnLimpiar_Click);
            // 
            // btnCompilar
            // 
            this.btnCompilar.Location = new System.Drawing.Point(174, 415);
            this.btnCompilar.Name = "btnCompilar";
            this.btnCompilar.Size = new System.Drawing.Size(75, 23);
            this.btnCompilar.TabIndex = 5;
            this.btnCompilar.Text = "Compilar";
            this.btnCompilar.UseVisualStyleBackColor = true;
            this.btnCompilar.Click += new System.EventHandler(this.btnCompilar_Click);
            // 
            // tablaTablas
            // 
            this.tablaTablas.Controls.Add(this.tabPage1);
            this.tablaTablas.Controls.Add(this.tabPage2);
            this.tablaTablas.Controls.Add(this.tabPage3);
            this.tablaTablas.Controls.Add(this.tabPage4);
            this.tablaTablas.Controls.Add(this.tabPage5);
            this.tablaTablas.Location = new System.Drawing.Point(601, 75);
            this.tablaTablas.Name = "tablaTablas";
            this.tablaTablas.SelectedIndex = 4;
            this.tablaTablas.Size = new System.Drawing.Size(876, 322);
            this.tablaTablas.TabIndex = 6;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.dataTablaSimbolos);
            this.tabPage1.Location = new System.Drawing.Point(4, 24);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(868, 294);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Tabla Simbolos";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // dataTablaSimbolos
            // 
            this.dataTablaSimbolos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataTablaSimbolos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.categoria,
            this.lexema,
            this.nroLinea,
            this.posInicial,
            this.posFinal});
            this.dataTablaSimbolos.Location = new System.Drawing.Point(0, 0);
            this.dataTablaSimbolos.Name = "dataTablaSimbolos";
            this.dataTablaSimbolos.Size = new System.Drawing.Size(865, 291);
            this.dataTablaSimbolos.TabIndex = 0;
            this.dataTablaSimbolos.Text = "dataGridView1";
            this.dataTablaSimbolos.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataTablaSimbolos_CellContentClick);
            // 
            // categoria
            // 
            this.categoria.HeaderText = "Categoría";
            this.categoria.Name = "categoria";
            this.categoria.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // lexema
            // 
            this.lexema.HeaderText = "Lexema";
            this.lexema.Name = "lexema";
            this.lexema.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // nroLinea
            // 
            this.nroLinea.HeaderText = "Nro Linea";
            this.nroLinea.Name = "nroLinea";
            // 
            // posInicial
            // 
            this.posInicial.HeaderText = "Pos Inicial";
            this.posInicial.Name = "posInicial";
            // 
            // posFinal
            // 
            this.posFinal.HeaderText = "Pos Final";
            this.posFinal.Name = "posFinal";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.dataTablaError);
            this.tabPage2.Location = new System.Drawing.Point(4, 24);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(868, 294);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Tabla Errores";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // dataTablaError
            // 
            this.dataTablaError.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataTablaError.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.lexemaError,
            this.nroLineaError,
            this.posInicialError,
            this.posFinalError,
            this.falla,
            this.causa,
            this.solucion});
            this.dataTablaError.Location = new System.Drawing.Point(2, 2);
            this.dataTablaError.Name = "dataTablaError";
            this.dataTablaError.Size = new System.Drawing.Size(865, 291);
            this.dataTablaError.TabIndex = 0;
            this.dataTablaError.Text = "dataGridView1";
            this.dataTablaError.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataTablaError_CellContentClick);
            // 
            // lexemaError
            // 
            this.lexemaError.HeaderText = "Lexema Error";
            this.lexemaError.Name = "lexemaError";
            // 
            // nroLineaError
            // 
            this.nroLineaError.HeaderText = "Nro Linea Error";
            this.nroLineaError.Name = "nroLineaError";
            // 
            // posInicialError
            // 
            this.posInicialError.HeaderText = "Pos Inicial Error";
            this.posInicialError.Name = "posInicialError";
            // 
            // posFinalError
            // 
            this.posFinalError.HeaderText = "Pos Final Error";
            this.posFinalError.Name = "posFinalError";
            // 
            // falla
            // 
            this.falla.HeaderText = "Falla";
            this.falla.Name = "falla";
            // 
            // causa
            // 
            this.causa.HeaderText = "Causa";
            this.causa.Name = "causa";
            // 
            // solucion
            // 
            this.solucion.HeaderText = "Solución";
            this.solucion.Name = "solucion";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.dataTablaDummy);
            this.tabPage3.Location = new System.Drawing.Point(4, 24);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(868, 294);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Tabla Dummy";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // dataTablaDummy
            // 
            this.dataTablaDummy.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataTablaDummy.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.categoriaDummy,
            this.lexemaDummy,
            this.nroLineaDummy,
            this.posInicialDummy,
            this.posFinalDummy,
            this.dummy});
            this.dataTablaDummy.Location = new System.Drawing.Point(1, 2);
            this.dataTablaDummy.Name = "dataTablaDummy";
            this.dataTablaDummy.Size = new System.Drawing.Size(871, 291);
            this.dataTablaDummy.TabIndex = 0;
            this.dataTablaDummy.Text = "dataGridView1";
            this.dataTablaDummy.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataTablaDummy_CellContentClick);
            // 
            // categoriaDummy
            // 
            this.categoriaDummy.HeaderText = "Categoría";
            this.categoriaDummy.Name = "categoriaDummy";
            // 
            // lexemaDummy
            // 
            this.lexemaDummy.HeaderText = "Lexema";
            this.lexemaDummy.Name = "lexemaDummy";
            // 
            // nroLineaDummy
            // 
            this.nroLineaDummy.HeaderText = "Nro Linea";
            this.nroLineaDummy.Name = "nroLineaDummy";
            // 
            // posInicialDummy
            // 
            this.posInicialDummy.HeaderText = "Pos Inicial";
            this.posInicialDummy.Name = "posInicialDummy";
            // 
            // posFinalDummy
            // 
            this.posFinalDummy.HeaderText = "Pos Final";
            this.posFinalDummy.Name = "posFinalDummy";
            // 
            // dummy
            // 
            this.dummy.HeaderText = "Dummy";
            this.dummy.Name = "dummy";
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.dataTablaLiterales);
            this.tabPage4.Location = new System.Drawing.Point(4, 24);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(868, 294);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Tabla Literales";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // dataTablaLiterales
            // 
            this.dataTablaLiterales.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataTablaLiterales.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.categoriaLiteral,
            this.lexemaLiteral,
            this.nroLineaLiteral,
            this.posInicialLietral,
            this.posFinalLiteral});
            this.dataTablaLiterales.Location = new System.Drawing.Point(1, 2);
            this.dataTablaLiterales.Name = "dataTablaLiterales";
            this.dataTablaLiterales.Size = new System.Drawing.Size(864, 291);
            this.dataTablaLiterales.TabIndex = 0;
            this.dataTablaLiterales.Text = "dataGridView1";
            this.dataTablaLiterales.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataTablaLiterales_CellContentClick);
            // 
            // categoriaLiteral
            // 
            this.categoriaLiteral.HeaderText = "Categoría";
            this.categoriaLiteral.Name = "categoriaLiteral";
            // 
            // lexemaLiteral
            // 
            this.lexemaLiteral.HeaderText = "Lexema";
            this.lexemaLiteral.Name = "lexemaLiteral";
            // 
            // nroLineaLiteral
            // 
            this.nroLineaLiteral.HeaderText = "Nro Linea";
            this.nroLineaLiteral.Name = "nroLineaLiteral";
            // 
            // posInicialLietral
            // 
            this.posInicialLietral.HeaderText = "Pos Inicial";
            this.posInicialLietral.Name = "posInicialLietral";
            // 
            // posFinalLiteral
            // 
            this.posFinalLiteral.HeaderText = "Pos FInal";
            this.posFinalLiteral.Name = "posFinalLiteral";
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.dataPalabrasReservadas);
            this.tabPage5.Location = new System.Drawing.Point(4, 24);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(868, 294);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Palabras Reservadas";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // dataPalabrasReservadas
            // 
            this.dataPalabrasReservadas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataPalabrasReservadas.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.categoriaReservadas,
            this.lexemaReservadas,
            this.nroLineaReservadas,
            this.posInicialReservadas,
            this.posFinalReservadas});
            this.dataPalabrasReservadas.Location = new System.Drawing.Point(1, 2);
            this.dataPalabrasReservadas.Name = "dataPalabrasReservadas";
            this.dataPalabrasReservadas.Size = new System.Drawing.Size(864, 291);
            this.dataPalabrasReservadas.TabIndex = 0;
            this.dataPalabrasReservadas.Text = "dataGridView1";
            this.dataPalabrasReservadas.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataPalabrasReservadas_CellContentClick);
            // 
            // categoriaReservadas
            // 
            this.categoriaReservadas.HeaderText = "Categoría";
            this.categoriaReservadas.Name = "categoriaReservadas";
            // 
            // lexemaReservadas
            // 
            this.lexemaReservadas.HeaderText = "Lexema";
            this.lexemaReservadas.Name = "lexemaReservadas";
            // 
            // nroLineaReservadas
            // 
            this.nroLineaReservadas.HeaderText = "Nro Linea";
            this.nroLineaReservadas.Name = "nroLineaReservadas";
            // 
            // posInicialReservadas
            // 
            this.posInicialReservadas.HeaderText = "Pos Inicial";
            this.posInicialReservadas.Name = "posInicialReservadas";
            // 
            // posFinalReservadas
            // 
            this.posFinalReservadas.HeaderText = "Pos Final";
            this.posFinalReservadas.Name = "posFinalReservadas";
            // 
            // btnTablaSimbolos
            // 
            this.btnTablaSimbolos.Location = new System.Drawing.Point(605, 415);
            this.btnTablaSimbolos.Name = "btnTablaSimbolos";
            this.btnTablaSimbolos.Size = new System.Drawing.Size(103, 23);
            this.btnTablaSimbolos.TabIndex = 7;
            this.btnTablaSimbolos.Text = "Tabla Simbolos";
            this.btnTablaSimbolos.UseVisualStyleBackColor = true;
            this.btnTablaSimbolos.Click += new System.EventHandler(this.btnTablaSimbolos_Click);
            // 
            // btnTablaErrores
            // 
            this.btnTablaErrores.Location = new System.Drawing.Point(714, 415);
            this.btnTablaErrores.Name = "btnTablaErrores";
            this.btnTablaErrores.Size = new System.Drawing.Size(102, 23);
            this.btnTablaErrores.TabIndex = 8;
            this.btnTablaErrores.Text = "Tabla Errores";
            this.btnTablaErrores.UseVisualStyleBackColor = true;
            this.btnTablaErrores.Click += new System.EventHandler(this.btnTablaErrores_Click);
            // 
            // btnTablaDummy
            // 
            this.btnTablaDummy.Location = new System.Drawing.Point(822, 415);
            this.btnTablaDummy.Name = "btnTablaDummy";
            this.btnTablaDummy.Size = new System.Drawing.Size(92, 23);
            this.btnTablaDummy.TabIndex = 9;
            this.btnTablaDummy.Text = "Tabla Dummy";
            this.btnTablaDummy.UseVisualStyleBackColor = true;
            this.btnTablaDummy.Click += new System.EventHandler(this.btnTablaDummy_Click);
            // 
            // btnTablaLiterales
            // 
            this.btnTablaLiterales.Location = new System.Drawing.Point(920, 415);
            this.btnTablaLiterales.Name = "btnTablaLiterales";
            this.btnTablaLiterales.Size = new System.Drawing.Size(94, 23);
            this.btnTablaLiterales.TabIndex = 10;
            this.btnTablaLiterales.Text = "Tabla Literales";
            this.btnTablaLiterales.UseVisualStyleBackColor = true;
            this.btnTablaLiterales.Click += new System.EventHandler(this.btnTablaLiterales_Click);
            // 
            // btnPalabrasReservadas
            // 
            this.btnPalabrasReservadas.Location = new System.Drawing.Point(1020, 415);
            this.btnPalabrasReservadas.Name = "btnPalabrasReservadas";
            this.btnPalabrasReservadas.Size = new System.Drawing.Size(122, 23);
            this.btnPalabrasReservadas.TabIndex = 11;
            this.btnPalabrasReservadas.Text = "Palabras Reservadas";
            this.btnPalabrasReservadas.UseVisualStyleBackColor = true;
            this.btnPalabrasReservadas.Click += new System.EventHandler(this.btnPalabrasReservadas_Click);
            // 
            // temporal
            // 
            this.temporal.BackColor = System.Drawing.SystemColors.Control;
            this.temporal.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.temporal.Location = new System.Drawing.Point(1389, 467);
            this.temporal.Name = "temporal";
            this.temporal.Size = new System.Drawing.Size(100, 16);
            this.temporal.TabIndex = 12;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "Categoría";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "Lexema";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "Nro Linea";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.HeaderText = "Pos Inicial";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.HeaderText = "Pos Final";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.HeaderText = "Categoría";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.HeaderText = "Lexema";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.HeaderText = "Nro Linea";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.HeaderText = "Pos Inicial";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.HeaderText = "Pos Final";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(20, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 15);
            this.label1.TabIndex = 13;
            this.label1.Text = "Archivo";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1489, 482);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.temporal);
            this.Controls.Add(this.btnPalabrasReservadas);
            this.Controls.Add(this.btnTablaLiterales);
            this.Controls.Add(this.btnTablaDummy);
            this.Controls.Add(this.btnTablaErrores);
            this.Controls.Add(this.btnTablaSimbolos);
            this.Controls.Add(this.tablaTablas);
            this.Controls.Add(this.btnCompilar);
            this.Controls.Add(this.btnLimpiar);
            this.Controls.Add(this.btnLeer);
            this.Controls.Add(this.txtTextoArchivo);
            this.Controls.Add(this.txtTextoConsola);
            this.Controls.Add(this.btnCargarArchivo);
            this.Controls.Add(this.txtNombreArchivo);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tablaTablas.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataTablaSimbolos)).EndInit();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataTablaError)).EndInit();
            this.tabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataTablaDummy)).EndInit();
            this.tabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataTablaLiterales)).EndInit();
            this.tabPage5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataPalabrasReservadas)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtNombreArchivo;
        private System.Windows.Forms.Button btnCargarArchivo;
        private System.Windows.Forms.TextBox txtTextoConsola;
        private System.Windows.Forms.TextBox txtTextoArchivo;
        private System.Windows.Forms.Button btnLeer;
        private System.Windows.Forms.Button btnLimpiar;
        private System.Windows.Forms.Button btnCompilar;
        private System.Windows.Forms.TabControl tablaTablas;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.DataGridView dataTablaSimbolos;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.DataGridView dataTablaDummy;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.DataGridView dataTablaLiterales;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.DataGridView dataPalabrasReservadas;
        private System.Windows.Forms.Button btnTablaSimbolos;
        private System.Windows.Forms.Button btnTablaErrores;
        private System.Windows.Forms.Button btnTablaDummy;
        private System.Windows.Forms.Button btnTablaLiterales;
        private System.Windows.Forms.Button btnPalabrasReservadas;
        private System.Windows.Forms.DataGridViewTextBoxColumn categoria;
        private System.Windows.Forms.DataGridViewTextBoxColumn lexema;
        private System.Windows.Forms.DataGridViewTextBoxColumn nroLinea;
        private System.Windows.Forms.DataGridViewTextBoxColumn posInicial;
        private System.Windows.Forms.DataGridViewTextBoxColumn posFinal;
        private System.Windows.Forms.DataGridView dataTablaError;
        private System.Windows.Forms.DataGridViewTextBoxColumn lexemaError;
        private System.Windows.Forms.DataGridViewTextBoxColumn nroLineaError;
        private System.Windows.Forms.DataGridViewTextBoxColumn posInicialError;
        private System.Windows.Forms.DataGridViewTextBoxColumn posFinalError;
        private System.Windows.Forms.DataGridViewTextBoxColumn falla;
        private System.Windows.Forms.DataGridViewTextBoxColumn causa;
        private System.Windows.Forms.DataGridViewTextBoxColumn solucion;
        private System.Windows.Forms.DataGridViewTextBoxColumn categoriaDummy;
        private System.Windows.Forms.DataGridViewTextBoxColumn lexemaDummy;
        private System.Windows.Forms.DataGridViewTextBoxColumn nroLineaDummy;
        private System.Windows.Forms.DataGridViewTextBoxColumn posInicialDummy;
        private System.Windows.Forms.DataGridViewTextBoxColumn posFinalDummy;
        private System.Windows.Forms.DataGridViewTextBoxColumn dummy;
        private System.Windows.Forms.DataGridViewTextBoxColumn categoriaLiteral;
        private System.Windows.Forms.DataGridViewTextBoxColumn lexemaLiteral;
        private System.Windows.Forms.DataGridViewTextBoxColumn nroLineaLiteral;
        private System.Windows.Forms.DataGridViewTextBoxColumn posInicialLietral;
        private System.Windows.Forms.DataGridViewTextBoxColumn posFinalLiteral;
        private System.Windows.Forms.DataGridViewTextBoxColumn categoriaReservadas;
        private System.Windows.Forms.DataGridViewTextBoxColumn lexemaReservadas;
        private System.Windows.Forms.DataGridViewTextBoxColumn nroLineaReservadas;
        private System.Windows.Forms.DataGridViewTextBoxColumn posInicialReservadas;
        private System.Windows.Forms.DataGridViewTextBoxColumn posFinalReservadas;
        private System.Windows.Forms.TextBox temporal;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.Label label1;
    }
}


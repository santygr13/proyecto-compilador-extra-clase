﻿using CompiladorExtraClaseEquipo.AnalisisLexico;
using CompiladorExtraClaseEquipo.ManejadorDeErrores;
using CompiladorExtraClaseEquipo.TablaSimbolo;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace CompiladorExtraClaseEquipo.AnalisisSintactico
{
    public class AnalizadorSintactico
    {
        private ComponenteLexico componente;
        private AnalizadorLexico analizador = new AnalizadorLexico();
        String pilaLlamados = "";

        public void Analizar(){

                PedirComponente();
                //componente= analizador.FormarComponente();
                Query("..");
                if (GestorErrores.HayErrores())
                {
                    MessageBox.Show("El programa no esta bien escrito ");
                }
                else if(Categoria.FIN_ARCHIVO.Equals(componente.ObtenerCategoria()))
                {
                  MessageBox.Show("Programa se compilo de manera satisfactoria");
                }
            else
            {
                MessageBox.Show("El programa no se compilo de forma satisfactoria");
            }
        }

        private void Query(String identacion) {

            if (Categoria.SELECT.Equals(componente.ObtenerCategoria()) )
            {
                DepurarEntrada(identacion, "<SELECT>");
                PedirComponente();
                campos();
                DepurarSalida(identacion, "<SELECT>");
                
                if (Categoria.FROM.Equals(componente.ObtenerCategoria()) )
                {
                    DepurarEntrada(identacion, "<FROM>");
                    PedirComponente();
                    tablas();
                    DepurarSalida(identacion, "<FROM>");
                    
                    if (Categoria.WHERE.Equals(componente.ObtenerCategoria()) )
                    {
                        DepurarEntrada(identacion, "<WHERE>");
                        PedirComponente();
                        condiciones();
                        DepurarSalida(identacion, "<WHERE>");
                        if (Categoria.ORDER.Equals(componente.ObtenerCategoria()))
                        {
                            DepurarEntrada(identacion, "<ORDER BY>");
                            PedirComponente();
                            if (Categoria.BY.Equals(componente.ObtenerCategoria()))
                            {
                                PedirComponente();
                                ordenadores();
                                DepurarSalida(identacion, "<ORDER BY>");
                            }
                            else
                            {
                                //Manejador de errores from
                                Error error = Error.CrearErrorSintactico(componente.ObtenerLexema(), componente.ObtenerNumeroLinea(), componente.ObtenerPosicionInicial(),
                             componente.ObtenerPosicionFinal(), "Palabra reservada no valida", $"Leí {componente.ObtenerLexema()}",
                             "Se esperaba un BY (es obligatorio)");

                                GestorErrores.Reportar(error);
                                throw new Exception("Se ha presentado un error sintactico que detiene el proceso. " +
                                     "Por favor validar la consola de errores y solucionar el problema para realizar nuevamente el proceso" +
                                     "de compilación..."); 
                            }


                        } 
                    }
                    if (Categoria.ORDER.Equals(componente.ObtenerCategoria()))
                    {
                        DepurarEntrada(identacion, "<ORDER>");
                        PedirComponente();
                        if (Categoria.BY.Equals(componente.ObtenerCategoria()))
                        {
                            PedirComponente();
                            ordenadores();
                            DepurarSalida(identacion, "<ORDER>");
                        }
                        else
                        {
                            //Manejador de errores from
                            Error error = Error.CrearErrorSintactico(componente.ObtenerLexema(), componente.ObtenerNumeroLinea(), componente.ObtenerPosicionInicial(),
                         componente.ObtenerPosicionFinal(), "Palabra reservada no valida", $"Leí {componente.ObtenerLexema()}",
                         "Se esperaba un BY (es obligatorio)");

                            GestorErrores.Reportar(error);
                            throw new Exception("Se ha presentado un error sintactico que detiene el proceso. " +
                                 "Por favor validar la consola de errores y solucionar el problema para realizar nuevamente el proceso" +
                                 "de compilación...");
                        }


                    }
                }
                else
                {
                    //Manejador de errores from
                    Error error = Error.CrearErrorSintactico(componente.ObtenerLexema(), componente.ObtenerNumeroLinea(), componente.ObtenerPosicionInicial(),
                    componente.ObtenerPosicionFinal(), "Sintaxis incompleta", "La palabra reservada from es obligatoria",
                    "Se esperaba un from");

                    GestorErrores.Reportar(error);
                    /*throw new Exception("Se ha presentado un error sintactico que detiene el proceso. " +
                         "Por favor validar la consola de errores y solucionar el problema para realizar nuevamente el proceso" +
                         "de compilación..."); */
                    MessageBox.Show("El campo no se digito de manera correcta o no se digito el from...");
                }
            }
            else
            {
                //Manejador de errores select
                Error error = Error.CrearErrorSintactico(componente.ObtenerLexema(), componente.ObtenerNumeroLinea(), componente.ObtenerPosicionInicial(),
                  componente.ObtenerPosicionFinal(), "Sintaxis incompleta", "La palabra reservada from es obligatoria",
                  "Se esperaba un select");

                GestorErrores.Reportar(error);
                throw new Exception("Se ha presentado un error sintactico que detiene el proceso. " +
                     "Por favor validar la consola de errores y solucionar el problema para realizar nuevamente el proceso" +
                     "de compilación..."); 
            }
            
        }

         public void PedirComponente()
         {

            if (componente == null)
            {
                analizador.EstablecerLineaActual(Cachee.Cache.ObtenerLinea(1));
                componente = analizador.FormarComponente();
            }
            else
            {
                analizador.EstablecerLineaActual(Cachee.Cache.ObtenerLinea(componente.ObtenerNumeroLinea()));
                componente = analizador.FormarComponente();
            }

         }

        private void campos()
        {
            if (Categoria.CAMPO.Equals(componente.ObtenerCategoria()))
            {
                DepurarEntrada("..", "<CAMPO>");
                PedirComponente();
                if (Categoria.CONCATENADOR.Equals(componente.ObtenerCategoria()))
                {
                    DepurarEntrada("..", "<CONCATENADOR>");
                    PedirComponente();
                    DepurarSalida("..", "<CONCATENADOR>");
                    if (Categoria.FROM.Equals(componente.ObtenerCategoria()))
                    {
                        Error error = Error.CrearErrorSintactico(componente.ObtenerLexema(), componente.ObtenerNumeroLinea(), componente.ObtenerPosicionInicial(),
                        componente.ObtenerPosicionFinal(), "Sintaxis incompleta", $"Leí {componente.ObtenerLexema()}",
                        "Se esperaba un campo ");

                        GestorErrores.Reportar(error);
                        MessageBox.Show("Debes especificar el campo que deseas concatenar luego de la coma.");
                    }
                    else
                    {
                        campos();
                    }
                }
                DepurarSalida("..", "<CAMPO>");
            }
            else
            {
                //Manejador de errores from
                Error error = Error.CrearErrorSintactico(componente.ObtenerLexema(), componente.ObtenerNumeroLinea(), componente.ObtenerPosicionInicial(),
                componente.ObtenerPosicionFinal(), "Sintanxis incompleta", "No lei un campo",
                "El campo es obligatorio");

                GestorErrores.Reportar(error);
                /*throw new Exception("Se ha presentado un error sintactico que detiene el proceso. " +
                     "Por favor validar la consola de errores y solucionar el problema para realizar nuevamente el proceso" +
                     "de compilación...");*/
                MessageBox.Show("El campo es obligatorio");
            }

        }

        private void tablas()
        {
            if (Categoria.TABLA.Equals(componente.ObtenerCategoria()))
            {
                DepurarEntrada("..", "<TABLA>");
                PedirComponente();
                if (Categoria.CONCATENADOR.Equals(componente.ObtenerCategoria()))
                {
                    DepurarEntrada("..", "<CONCATENADOR>");
                    PedirComponente();
                    DepurarSalida("..", "<CONCATENADOR>");

                    if (Categoria.FIN_ARCHIVO.Equals(componente.ObtenerCategoria()))
                    {
                        Error error = Error.CrearErrorSintactico(componente.ObtenerLexema(), componente.ObtenerNumeroLinea(), componente.ObtenerPosicionInicial(),
                        componente.ObtenerPosicionFinal(), "Sintaxis incompleta", $"Leí {componente.ObtenerLexema()}",
                        "Se esperaba una tabla ");

                        GestorErrores.Reportar(error);

                        MessageBox.Show("Debes especificar la tabla que deseas concatenar luego de la coma.");
                    }
                    else if (Categoria.WHERE.Equals(componente.ObtenerCategoria()))
                    {
                        Error error = Error.CrearErrorSintactico(componente.ObtenerLexema(), componente.ObtenerNumeroLinea(), componente.ObtenerPosicionInicial(),
                        componente.ObtenerPosicionFinal(), "Sintaxis incompleta", $"Leí {componente.ObtenerLexema()}",
                        "Se esperaba una tabla ");

                        GestorErrores.Reportar(error);
                        MessageBox.Show("Debes especificar la tabla que deseas concatenar luego de la coma.");
                    }else if (Categoria.ORDER.Equals(componente.ObtenerCategoria()))
                    {
                        Error error = Error.CrearErrorSintactico(componente.ObtenerLexema(), componente.ObtenerNumeroLinea(), componente.ObtenerPosicionInicial(),
                        componente.ObtenerPosicionFinal(), "Sintaxis incompleta", $"Leí {componente.ObtenerLexema()}",
                        "Se esperaba una tabla ");

                        GestorErrores.Reportar(error);
                        MessageBox.Show("Debes especificar la tabla que deseas concatenar luego de la coma.");
                    }
                    else
                    {
                        tablas();
                    }
                }
                DepurarSalida("..", "<TABLA>");
            }
            else
            {
                //Manejador de errores from
                Error error = Error.CrearErrorSintactico(componente.ObtenerLexema(), componente.ObtenerNumeroLinea(), componente.ObtenerPosicionInicial(),
                componente.ObtenerPosicionFinal(), "Sintanxis incompleta", "No lei una tabla",
                "La tabla es obligatoria");

                GestorErrores.Reportar(error);
                /*throw new Exception("Se ha presentado un error sintactico que detiene el proceso. " +
                     "Por favor validar la consola de errores y solucionar el problema para realizar nuevamente el proceso" +
                     "de compilación...");*/
                MessageBox.Show("La tabla es obligatoria");
            }
        }



        private void condiciones()
        {
            DepurarEntrada("..", "<CONDICIONES>");
            condicion();

            if(Categoria.AND.Equals(componente.ObtenerCategoria()))
            {
                Conector();
                condicion();
                // restoCondicion();
            }
            if (Categoria.OR.Equals(componente.ObtenerCategoria()))
            {
                Conector();
                condicion();
                // restoCondicion();
            }
            DepurarSalida("..", "<CONDICIONES>");
        }

        private void condicion()
        {
            DepurarEntrada("..", "<OPERANDO>");
            operando();
            DepurarSalida("..", "<OPERANDO>");
            DepurarEntrada("..", "<OPERADOR>");
            operador();
            
            DepurarSalida("..", "<OPERADOR>");
            DepurarEntrada("..", "<OPERANDO>");
            operando();
            DepurarSalida("..", "<OPERANDO>");
        }

        private void operando()
        {
            
            if (Categoria.CAMPO.Equals(componente.ObtenerCategoria()) )
            {
                PedirComponente();
            }
            else if (Categoria.LITERAL.Equals(componente.ObtenerCategoria()))
            {
                PedirComponente();
            }
            else if (Categoria.NUMERO_ENTERO.Equals(componente.ObtenerCategoria()))
            {
                PedirComponente();
            }
            else if (Categoria.NUMERO_DECIMAL.Equals(componente.ObtenerCategoria()))
            {
                PedirComponente();
            }
            else
            {
                //Manejador de errores
                Error error = Error.CrearErrorSintactico(componente.ObtenerLexema(), componente.ObtenerNumeroLinea(), componente.ObtenerPosicionInicial(),
                   componente.ObtenerPosicionFinal(), "Operando incorrecto", $"Leí {componente.ObtenerLexema()}",
                   "Se esperaba un CAMPO O LITERAL O NUMRO ENTERO Y/O DECIMAL, no pueden haber dos operadores juntos");

                GestorErrores.Reportar(error);
                throw new Exception("Se ha presentado un error sintactico que detiene el proceso. " +
                     "Por favor validar la consola de errores y solucionar el problema para realizar nuevamente el proceso" +
                     "de compilación...");
            }
        }

        private void operador()
        {
            if (Categoria.MAYOR_QUE.Equals(componente.ObtenerCategoria()))
            {
                PedirComponente();
            }
            else if (Categoria.MENOR_QUE.Equals(componente.ObtenerCategoria()))
            {
                PedirComponente();
            }
            else if (Categoria.IGUAL_QUE.Equals(componente.ObtenerCategoria()))
            {
                PedirComponente();
            }
            else if (Categoria.MAYOR_IGUAL_QUE.Equals(componente.ObtenerCategoria()))
            {
                PedirComponente();
            }
            else if (Categoria.MENOR_IGUAL_QUE.Equals(componente.ObtenerCategoria()))
            {
                PedirComponente();
            }
            else if (Categoria.DIFERENTE_QUE.Equals(componente.ObtenerCategoria()))
            {
                PedirComponente();
            }
            else
            {
                //Manejador de errores
                Error error = Error.CrearErrorSintactico(componente.ObtenerLexema(), componente.ObtenerNumeroLinea(), componente.ObtenerPosicionInicial(),
                   componente.ObtenerPosicionFinal(), "Operador incorrecto", $"Leí {componente.ObtenerLexema()}",
                   "Se esperaba un MAYOR O MENOR O MAYOR QUE O MENOR QUE O IGUAL QUE O DIFERENTE QUE  ");

                GestorErrores.Reportar(error);
                throw new Exception("Se ha presentado un error sintactico que detiene el proceso. " +
                     "Por favor validar la consola de errores y solucionar el problema para realizar nuevamente el proceso" +
                     "de compilación...");
            }
        }


        public void Conector()
        {
            DepurarEntrada("..", "<CONECTOR>");
            if (Categoria.AND.Equals(componente.ObtenerCategoria()))
            {
                PedirComponente();
            }
            else if (Categoria.OR.Equals(componente.ObtenerCategoria()))
            {
                PedirComponente();
            }
            DepurarSalida("..", "<CONECTOR>");
        }

        private void ordenadores()
        {
            DepurarEntrada("..", "<ORDENADOR>");
            //PedirComponente();
            if (Categoria.CAMPO.Equals(componente.ObtenerCategoria()))
            {
                campos();
                if (Categoria.ASC.Equals(componente.ObtenerCategoria()) || Categoria.DESC.Equals(componente.ObtenerCategoria()))
                {
                    Criterio();
                }
            }else if (Categoria.NUMERO_ENTERO.Equals(componente.ObtenerCategoria()))
            {
                Indices();
                if (Categoria.ASC.Equals(componente.ObtenerCategoria()) || Categoria.DESC.Equals(componente.ObtenerCategoria()))
                {
                    Criterio();
                }
                
            }
            else
            {
                Error error = Error.CrearErrorSintactico(componente.ObtenerLexema(), componente.ObtenerNumeroLinea(), componente.ObtenerPosicionInicial(),
                   componente.ObtenerPosicionFinal(), "Operador incorrecto", $"Leí {componente.ObtenerLexema()}",
                   "Se esperaba un indice o un campo");

                GestorErrores.Reportar(error);
                throw new Exception("Se ha presentado un error sintactico que detiene el proceso. " +
                     "Por favor validar la consola de errores y solucionar el problema para realizar nuevamente el proceso" +
                     "de compilación...");
            }
            DepurarSalida("..", "<ORDENADOR>");

            
        }

        private void Indices()
        {
            if (Categoria.NUMERO_ENTERO.Equals(componente.ObtenerCategoria()))
            {
                DepurarEntrada("..", "<INDICE>");
                PedirComponente();
                if (Categoria.CONCATENADOR.Equals(componente.ObtenerCategoria()))
                {
                    DepurarEntrada("..", "<CONCATENADOR>");
                    PedirComponente();
                    DepurarSalida("..", "<CONCATENADOR>");
                    if (Categoria.NUMERO_ENTERO.Equals(componente.ObtenerCategoria()))
                    {
                        DepurarEntrada("..", "<Indices>");
                        Indices();
                        DepurarSalida("..", "<Indices>");
                    }/*else if (Categoria.CAMPO.Equals(componente.ObtenerCategoria()))
                    {
                        campos();
                    }*/
                    else
                    {

                        Error error = Error.CrearErrorSintactico(componente.ObtenerLexema(), componente.ObtenerNumeroLinea(), componente.ObtenerPosicionInicial(),
                        componente.ObtenerPosicionFinal(), "Sintaxis incompleta", $"Leí {componente.ObtenerLexema()}",
                        "Se esperaba un numero entero");

                        GestorErrores.Reportar(error);
                        MessageBox.Show("Luego de una coma se debe poner otro indice {NUMERO ENTERO}");
                    }

                }
            }
        }

        public void Criterio()
        {
            if (Categoria.ASC.Equals(componente.ObtenerCategoria()))
            {
                DepurarEntrada("..", "<ASC>");
                PedirComponente();
                DepurarSalida("..", "ASC");
            }
            else if (Categoria.DESC.Equals(componente.ObtenerCategoria()))
            {
                DepurarEntrada("..", "<DESC>");
                PedirComponente();
                DepurarSalida("..", "DESC");
            }
            else
            {
                //Manejador de errores
                Error error = Error.CrearErrorSintactico(componente.ObtenerLexema(), componente.ObtenerNumeroLinea(), componente.ObtenerPosicionInicial(),
                    componente.ObtenerPosicionFinal(), "Criterio incorrecto", $"Leí {componente.ObtenerLexema()}",
                    "Se esperaba ASC O DESC");

                GestorErrores.Reportar(error);
                throw new Exception("Se ha presentado un error sintactico que detiene el proceso. " +
                     "Por favor validar la consola de errores y solucionar el problema para realizar nuevamente el proceso" +
                     "de compilación...");
            }
        }

        private void DepurarEntrada(String identacion, String regla)
        {
            pilaLlamados = pilaLlamados + identacion + "ENTRANDO A REGLA "
                + regla + "con lexema " + componente.ObtenerLexema() +
                " y categoria " + componente.ObtenerCategoria() + "\n";

            ImprimirTraza();
        }
        private void DepurarSalida(String identacion, String regla)
        {
            pilaLlamados = pilaLlamados + identacion + "SALIENDO A REGLA "
             + regla + "\n";

            ImprimirTraza();
        }
        private void ImprimirTraza()
        {
            MessageBox.Show(pilaLlamados);
        }


    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CompiladorExtraClaseEquipo.ManejadorDeErrores
{
    public class Error
    {
        private String Lexema;
        private int NumeroLinea;
        private int PosicionInicial;
        private int PosicionFinal;
        private String Falla;
        private String Causa;
        private String Solucion;
        private TipoError Tipo;

        private Error(String Lexema, int NumeroLinea, int PosicionInicial, int PosicionFinal, String Falla, String Causa, String Solucion, TipoError tipo)
        {
            this.Lexema = (Lexema == null) ? "" : Lexema;
            this.NumeroLinea = NumeroLinea;
            this.PosicionInicial = PosicionInicial;
            this.PosicionFinal = PosicionFinal;
            this.Tipo = Tipo;
            this.Falla = Falla;
            this.Causa = Causa;
            this.Solucion = Solucion;
        }

        public static Error CrearErrorLexico(String Lexema, int NumeroLinea, int PosicionInicial, int PosicionFinal, String Falla, String Causa, String Solucion)
        {
            return new Error(Lexema, NumeroLinea, PosicionInicial, PosicionFinal, Falla, Causa, Solucion, TipoError.LEXICO);

        }

        public static Error CrearErrorSintactico(String Lexema, int NumeroLinea, int PosicionInicial, int PosicionFinal, String Falla, String Causa, String Solucion)
        {
            return new Error(Lexema, NumeroLinea, PosicionInicial, PosicionFinal, Falla, Causa, Solucion, TipoError.SINTACTICO);

        }

        

        public String ObtenerLexema()
        {
            return Lexema;
        }

        public int ObtenerNumeroLinea()
        {
            return NumeroLinea;
        }

        public String ObtenerCausa()
        {
            return Causa;
        }
        public int ObtenerPosicionInicial()
        {
            return PosicionInicial;
        }

        public int ObtenerPosicionFinal()
        {
            return PosicionFinal;
        }

        public String ObtenerFalla()
        {
            return Falla;
        }

        public String ObtenerSolucion()
        {
            return Solucion;
        }

        public TipoError ObtenerTipoError()
        {
            return Tipo;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CompiladorExtraClaseEquipo.ManejadorDeErrores
{
    public class GestorErrores
    {
        private static Dictionary<TipoError, List<Error>> Errores = new Dictionary<TipoError, List<Error>>();

        public static List<Error> ObtenerErrores(TipoError tipo)
        {
            if (!Errores.ContainsKey(tipo))
            {
                Errores.Add(tipo, new List<Error>());
            }

            return Errores[tipo];
        }

        public static void Reportar(Error Error)
        {
            if (Error != null)
            {
                ObtenerErrores(Error.ObtenerTipoError()).Add(Error);
            }
        }

        public static bool HayErrores(TipoError Tipo)
        {
            return ObtenerErrores(Tipo).Count > 0;
        }

        public static bool HayErrores()
        {
            return HayErrores(TipoError.LEXICO) || HayErrores(TipoError.SINTACTICO);
        }

        public static List<Error> ObtenerTodosErrores()
        {
            return Errores.Values.SelectMany(Error => Error).ToList();
        }

        public static void Limpiar()
        {
            Errores.Clear();
        }


    }
}

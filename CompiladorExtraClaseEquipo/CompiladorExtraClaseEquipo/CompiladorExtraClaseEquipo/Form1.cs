﻿using CompiladorExtraClaseEquipo.AnalisisLexico;
using CompiladorExtraClaseEquipo.AnalisisSintactico;
using CompiladorExtraClaseEquipo.Cachee;
using CompiladorExtraClaseEquipo.ManejadorDeErrores;
using CompiladorExtraClaseEquipo.TablaSimbolo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CompiladorExtraClaseEquipo
{
    public partial class Form1 : Form
    {
        public OpenFileDialog openFileDialog1 = new OpenFileDialog();
        public Form1()
        {
            InitializeComponent();
        }

        public void Inicializar()
        {
            TablaDeDummys.Limpiar();
            TablaDeSimbolos.Limpiar();
            GestorErrores.Limpiar();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void btnCargarArchivo_Click(object sender, EventArgs e)
        {
            openFileDialog1 = new OpenFileDialog();
            openFileDialog1.Filter = "Cursor Files|*.txt";
            openFileDialog1.Title = "Buscar archivo";
            DialogResult resultado = openFileDialog1.ShowDialog();
            string[] lineas = { };
            List<string> listaLineas = new List<string>();

            if (resultado == DialogResult.OK)
            {
                string nombreArchivo = openFileDialog1.FileName;
                txtNombreArchivo.Text = nombreArchivo;

                using (StreamReader lector = new StreamReader(nombreArchivo))
                {
                    temporal.Clear();
                    txtTextoArchivo.Clear();
                    temporal.Text = lector.ReadToEnd();
                    string[] ln = temporal.Text.Split('\n');
                    ln[ln.Length - 1] += "\n";
                    if (temporal.Text != String.Empty)
                    {
                        for (int i = 0; i < ln.Length; i++)
                        {
                            string datos = temporal.Lines[i];
                            int numeroLinea = i + 1;
                            listaLineas.Add(datos);
                            txtTextoArchivo.Text += $"{numeroLinea} --> {datos} {Environment.NewLine}";
                            Cache.AgregarLinea(ln[i]);
                        }

                        Cache.ObtenerLinea(Cache.ObtenerLineas().Count).
                        ColocarContenido(Cache.ObtenerLinea(Cache.ObtenerLineas().Count).ObtenerContenido() + "\n");
                    }

                }

            }

        }

        private void btnLeer_Click(object sender, EventArgs e)
        {
            Cache.Limpiar();
            txtTextoArchivo.Clear();
            string[] linea = txtTextoConsola.Text.Split('\n');
            linea[linea.Length - 1] += "\n";
            List<string> listaLineas = new List<string>();

            for (int i = 0; i < linea.Length; i++)
            {
                string lineaCompleta = (i + 1).ToString() + "->" + linea[i];
                listaLineas.Add(lineaCompleta);
                Cache.AgregarLinea(linea[i]);

            }

            txtTextoArchivo.Text = String.Join(Environment.NewLine, listaLineas);
            txtTextoConsola.Clear();
        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            txtTextoArchivo.Clear();
            txtTextoConsola.Clear();
        }

        private void btnCompilar_Click(object sender, EventArgs e)
        {

            Inicializar();
            AnalizadorSintactico analizadorSintactico = new AnalizadorSintactico();
            analizadorSintactico.Analizar();

            /*
            AnalizadorLexico analizadorLexico = new AnalizadorLexico();
            analizadorLexico.EstablecerLineaActual(Cache.ObtenerLinea(1));
            ComponenteLexico componenteLexico = null;
            //string[] linea = { };
            do
            {
                componenteLexico = analizadorLexico.FormarComponente();
                MessageBox.Show(componenteLexico.ToString());

            } while (!componenteLexico.ObtenerCategoria().Equals(Categoria.FIN_ARCHIVO));
            */
            
            Cache.Limpiar();

        }

        private void btnTablaSimbolos_Click(object sender, EventArgs e)
        {
            dataTablaSimbolos.Rows.Clear();
            foreach (ComponenteLexico componenteLexicoo in TablaDeSimbolos.ObtenerTodosSimbolos())
            {
                dataTablaSimbolos.Rows.Add(componenteLexicoo.ObtenerCategoria(), componenteLexicoo.ObtenerLexema(), componenteLexicoo.ObtenerNumeroLinea(), componenteLexicoo.ObtenerPosicionInicial(),
                    componenteLexicoo.ObtenerPosicionFinal());
            }
            tablaTablas.SelectedIndex = 0;
        }

        private void btnTablaErrores_Click(object sender, EventArgs e)
        {
            dataTablaError.Rows.Clear();
            foreach (Error error in GestorErrores.ObtenerTodosErrores())
            {
                dataTablaError.Rows.Add(error.ObtenerLexema(), error.ObtenerNumeroLinea(), error.ObtenerPosicionInicial(), error.ObtenerPosicionFinal(),
                    error.ObtenerFalla(), error.ObtenerCausa(), error.ObtenerSolucion());
            }
            tablaTablas.SelectedIndex = 1;
        }

        private void btnTablaDummy_Click(object sender, EventArgs e)
        {
            dataTablaDummy.Rows.Clear();
            foreach (ComponenteLexico componenteLexicooo in TablaDeDummys.ObtenerTodosSimbolos())
            {
                dataTablaDummy.Rows.Add(componenteLexicooo.ObtenerCategoria(), componenteLexicooo.ObtenerLexema(), componenteLexicooo.ObtenerNumeroLinea(), componenteLexicooo.ObtenerPosicionInicial(),
                    componenteLexicooo.ObtenerPosicionFinal(), componenteLexicooo.ObtenerTipo());
            }
            tablaTablas.SelectedIndex = 2;
        }

        private void btnTablaLiterales_Click(object sender, EventArgs e)
        {
            dataTablaLiterales.Rows.Clear();
            foreach (ComponenteLexico componenteLexicooo in TablaDeLiterales.ObtenerTodosSimbolos())
            {
                dataTablaLiterales.Rows.Add(componenteLexicooo.ObtenerCategoria(), componenteLexicooo.ObtenerLexema(), componenteLexicooo.ObtenerNumeroLinea(), componenteLexicooo.ObtenerPosicionInicial(),
                    componenteLexicooo.ObtenerPosicionFinal(), componenteLexicooo.ObtenerTipo());
            }
            tablaTablas.SelectedIndex = 3;
        }

        private void btnPalabrasReservadas_Click(object sender, EventArgs e)
        {
            dataPalabrasReservadas.Rows.Clear();
            foreach (ComponenteLexico componenteLexicooo in TablaReservadas.ObtenerTodosSimbolos())
            {
                dataPalabrasReservadas.Rows.Add(componenteLexicooo.ObtenerCategoria(), componenteLexicooo.ObtenerLexema(), componenteLexicooo.ObtenerNumeroLinea(), componenteLexicooo.ObtenerPosicionInicial(),
                    componenteLexicooo.ObtenerPosicionFinal(), componenteLexicooo.ObtenerTipo());
            }
            tablaTablas.SelectedIndex = 4;
        }

        private void dataTablaSimbolos_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dataTablaError_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dataTablaDummy_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dataTablaLiterales_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dataPalabrasReservadas_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}

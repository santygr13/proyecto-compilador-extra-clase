﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CompiladorExtraClaseEquipo.Cachee
{
    public class Cache
    {
        
        private static List<Linea> Lineas = new List<Linea>();


        public static void Limpiar()
        {
            Lineas.Clear();
        }

        public static void Poblar(List<Linea> Lineas)
        {
            Limpiar();
            if (Lineas != null && Lineas.Count > 0)
            {
                Cache.Lineas = Lineas;
            }
        }

        public static void AgregarLinea(string Contenido)
        {
            if (Contenido != null)
            {
                int NumeroLinea = Lineas.Count + 1;

                Lineas.Add(Linea.Crear(NumeroLinea, Contenido));
            }
        }

        public static Linea ObtenerLinea(int NumeroLinea)
        {
            Linea LineaRetorno;
            if (ExisteLinea(NumeroLinea))
            {
                LineaRetorno = Lineas[NumeroLinea - 1];
            }
            else
            {
                LineaRetorno = Linea.Crear(Lineas.Count + 1, "@EOF@");
            }

            return LineaRetorno;
        }

        public static bool ExisteLinea(int NumeroLinea)
        {
            return (NumeroLinea > 0 && NumeroLinea <= Lineas.Count);
        }

        public static List<Linea> ObtenerLineas()
        {
            return Lineas;
        }

    }
}

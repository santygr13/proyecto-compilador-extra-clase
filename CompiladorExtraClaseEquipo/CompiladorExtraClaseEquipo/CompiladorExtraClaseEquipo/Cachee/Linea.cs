﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CompiladorExtraClaseEquipo.Cachee
{
    public class Linea
    {
        private int NroLinea;
        private String Contenido;

        private Linea(int Numero, String Contenido)
        {
            ColocarNumero(Numero);
            ColocarContenido(Contenido);
        }

        public static Linea Crear(int Numero, String Contenido)
        {
            return new Linea(Numero, Contenido);
        }

        public int ObtenerNumero()
        {
            return NroLinea;
        }

        public String ObtenerContenido()
        {
            return Contenido;
        }

        public void ColocarNumero(int numero)
        {
            this.NroLinea = numero;
        }

        public void ColocarContenido(String Contenido)
        {
            if (Contenido == null)
            {
                Contenido = "";
            }

            this.Contenido = Contenido;
        }

    }
}

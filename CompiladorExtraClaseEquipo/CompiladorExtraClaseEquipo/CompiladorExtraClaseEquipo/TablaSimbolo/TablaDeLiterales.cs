﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CompiladorExtraClaseEquipo.TablaSimbolo
{
    public class TablaDeLiterales
    {
        private static Dictionary<string, List<ComponenteLexico>> Simbolos = new Dictionary<string, List<ComponenteLexico>>();


        public static ComponenteLexico ComprobarLiteral(ComponenteLexico Componente)
        {

            ComponenteLexico Retorno = null;


            if (Componente != null && Categoria.NUMERO_DECIMAL.Equals(Componente.ObtenerCategoria()) || Categoria.NUMERO_ENTERO.Equals(Componente.ObtenerCategoria()) || Categoria.LITERAL.Equals(Componente.ObtenerCategoria()))
            {

                Retorno = ComponenteLexico.CrearLiteral(Componente.ObtenerCategoria(),
                    Componente.ObtenerLexema(), Componente.ObtenerNumeroLinea(), Componente.ObtenerPosicionInicial(), Componente.ObtenerPosicionFinal());
            }
            else
            {
                Retorno = Componente;
            }

            return Retorno;
        }


        public static void Agregar(ComponenteLexico Componente)
        {
            if (Componente != null && TipoComponente.LITERAL.Equals(Componente.ObtenerTipo()))
            {
                ObtenerSimbolo(Componente.ObtenerLexema()).Add(Componente);
            }
        }

        public static List<ComponenteLexico> ObtenerSimbolo(String Lexema)
        {
            if (!Simbolos.ContainsKey(Lexema))
            {
                Simbolos.Add(Lexema, new List<ComponenteLexico>());
            }

            return Simbolos[Lexema];
        }

        public static List<ComponenteLexico> ObtenerTodosSimbolos()
        {
            return Simbolos.Values.SelectMany(Componente => Componente).ToList();
        }

        public static void Limpiar()
        {
            Simbolos.Clear();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CompiladorExtraClaseEquipo.TablaSimbolo
{
    public class TablaReservadas
    {
        private static Dictionary<string, List<ComponenteLexico>> Simbolos = new Dictionary<string, List<ComponenteLexico>>();
        private static Dictionary<string, ComponenteLexico> PalabrasReservadasBase = new Dictionary<string, ComponenteLexico>();
        private static bool TablaInicializada = false;
        private static void Inicializar()
        {

            PalabrasReservadasBase.Add("AND", ComponenteLexico.CrearpalabraReservada(Categoria.AND, "AND"));
            PalabrasReservadasBase.Add("ASC", ComponenteLexico.CrearpalabraReservada(Categoria.ASC, "ASC"));
            PalabrasReservadasBase.Add("BY", ComponenteLexico.CrearpalabraReservada(Categoria.BY, "BY"));
            PalabrasReservadasBase.Add("DESC", ComponenteLexico.CrearpalabraReservada(Categoria.DESC, "DESC"));
            PalabrasReservadasBase.Add("FROM", ComponenteLexico.CrearpalabraReservada(Categoria.FROM, "FROM"));
            PalabrasReservadasBase.Add("OR", ComponenteLexico.CrearpalabraReservada(Categoria.OR, "OR"));
            PalabrasReservadasBase.Add("ORDER", ComponenteLexico.CrearpalabraReservada(Categoria.ORDER, "ORDER"));
            PalabrasReservadasBase.Add("SELECT", ComponenteLexico.CrearpalabraReservada(Categoria.SELECT, "SELECT"));
            PalabrasReservadasBase.Add("WHERE", ComponenteLexico.CrearpalabraReservada(Categoria.WHERE, "WHERE"));
            TablaInicializada = true;
        }

        public static ComponenteLexico ComprobarPalabraReservada(ComponenteLexico Componente)
        {

            ComponenteLexico Retorno = null;

            if (!TablaInicializada)
            {
                Inicializar();
            }
            if (Componente != null && Componente.ObtenerLexema() != null && Categoria.IDENTIFICADOR.Equals(Componente.ObtenerCategoria()) && PalabrasReservadasBase.ContainsKey(Componente.ObtenerLexema().ToUpper()))
            {

                Retorno = ComponenteLexico.CrearpalabraReservada(PalabrasReservadasBase[Componente.ObtenerLexema().ToUpper()].ObtenerCategoria(),
                    Componente.ObtenerLexema(), Componente.ObtenerNumeroLinea(), Componente.ObtenerPosicionInicial(), Componente.ObtenerPosicionFinal());
            }
            else
            {
                Retorno = Componente;
            }

            return Retorno;
        }
        public static void Agregar(ComponenteLexico Componente)
        {
            if (Componente != null && TipoComponente.PALABRA_RESERVADA.Equals(Componente.ObtenerTipo()))
            {
                ObtenerSimbolo(Componente.ObtenerLexema()).Add(Componente);
            }
        }

        public static List<ComponenteLexico> ObtenerSimbolo(String Lexema)
        {
            if (!Simbolos.ContainsKey(Lexema))
            {
                Simbolos.Add(Lexema, new List<ComponenteLexico>());
            }

            return Simbolos[Lexema];
        }

        public static List<ComponenteLexico> ObtenerTodosSimbolos()
        {
            return Simbolos.Values.SelectMany(Componente => Componente).ToList();
        }

        public static void Limpiar()
        {
            Simbolos.Clear();
        }
    }
}

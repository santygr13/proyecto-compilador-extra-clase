﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CompiladorExtraClaseEquipo.TablaSimbolo
{
    public enum TipoComponente
    {
        SIMBOLO, PALABRA_RESERVADA, DUMMY, LITERAL
    }
}

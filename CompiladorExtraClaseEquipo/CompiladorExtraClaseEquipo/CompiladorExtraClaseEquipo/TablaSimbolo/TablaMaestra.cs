﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CompiladorExtraClaseEquipo.TablaSimbolo
{
    public class TablaMaestra
    {
        public static void Agregar(ComponenteLexico Componente)
        {
            if (Componente != null)
            {
                Componente = TablaReservadas.ComprobarPalabraReservada(Componente);
                Componente = TablaDeLiterales.ComprobarLiteral(Componente);
                switch (Componente.ObtenerTipo())
                {
                    case TipoComponente.SIMBOLO:
                        TablaDeSimbolos.Agregar(Componente);
                        break;
                    case TipoComponente.PALABRA_RESERVADA:
                        TablaReservadas.Agregar(Componente);
                        break;
                    case TipoComponente.DUMMY:
                        TablaDeDummys.Agregar(Componente);
                        break;
                    case TipoComponente.LITERAL:
                        TablaDeLiterales.Agregar(Componente);
                        break;
                    default:
                        throw new Exception("Tipo de componente lexico no soportado");

                }
            }
        }

        public static List<ComponenteLexico> ObtenerCOmponentes(TipoComponente Componente)
        {
            switch (Componente)
            {
                case TipoComponente.SIMBOLO:
                    return TablaDeSimbolos.ObtenerTodosSimbolos();
                case TipoComponente.DUMMY:
                    return TablaDeDummys.ObtenerTodosSimbolos();
                case TipoComponente.PALABRA_RESERVADA:
                    return TablaReservadas.ObtenerTodosSimbolos();
                case TipoComponente.LITERAL:
                    return TablaDeLiterales.ObtenerTodosSimbolos();
                default:
                    throw new Exception("Tipo de componente lexico no soportado");

            }
        }

        public static void Limpiar()
        {
            TablaDeSimbolos.Limpiar();
            TablaDeDummys.Limpiar();
            TablaReservadas.Limpiar();
            TablaDeLiterales.Limpiar();
        }
    }
}

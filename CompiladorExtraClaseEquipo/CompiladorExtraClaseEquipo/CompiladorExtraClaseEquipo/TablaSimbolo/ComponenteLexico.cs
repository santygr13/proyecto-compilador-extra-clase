﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CompiladorExtraClaseEquipo.TablaSimbolo
{
    public class ComponenteLexico
    {
        private Categoria Categoria;
        private String Lexema;
        private int NumeroLinea;
        private int PosicionInicial;
        private int PosicionFinal;
        private TipoComponente Tipo;

        private ComponenteLexico(Categoria Categoria, String Lexema, int NumeroLinea, int PosicionInicial, int PosicionFinal, TipoComponente Tipo)
        {
            this.Categoria = Categoria;
            this.Lexema = (Lexema == null) ? "" : Lexema;
            this.NumeroLinea = NumeroLinea;
            this.PosicionInicial = PosicionInicial;
            this.PosicionFinal = PosicionFinal;
            this.Tipo = Tipo;

        }

        public static ComponenteLexico CrearSimbolo(Categoria Categoria, String Lexema, int NumeroLinea, int PosicionInicial, int PosicionFinal)
        {
            return new ComponenteLexico(Categoria, Lexema, NumeroLinea, PosicionInicial, PosicionFinal, TipoComponente.SIMBOLO);
        }

        public static ComponenteLexico CrearDummy(Categoria Categoria, String Lexema, int NumeroLinea, int PosicionInicial, int PosicionFinal)
        {
            return new ComponenteLexico(Categoria, Lexema, NumeroLinea, PosicionInicial, PosicionFinal, TipoComponente.DUMMY);
        }

        public static ComponenteLexico CrearpalabraReservada(Categoria Categoria, String Lexema)
        {
            return new ComponenteLexico(Categoria, Lexema, -1, -1, -1, TipoComponente.PALABRA_RESERVADA);
        }

        public static ComponenteLexico CrearpalabraReservada(Categoria Categoria, String Lexema, int NumeroLinea, int PosicionInicial, int PosicionFinal)
        {
            return new ComponenteLexico(Categoria, Lexema, NumeroLinea, PosicionInicial, PosicionFinal, TipoComponente.PALABRA_RESERVADA);
        }

        public static ComponenteLexico CrearLiteral(Categoria Categoria, String Lexema, int NumeroLinea, int PosicionInicial, int PosicionFinal)
        {
            return new ComponenteLexico(Categoria, Lexema, NumeroLinea, PosicionInicial, PosicionFinal, TipoComponente.LITERAL);
        }
        public Categoria ObtenerCategoria()
        {
            return Categoria;
        }

        public String ObtenerLexema()
        {
            return Lexema;
        }

        public int ObtenerNumeroLinea()
        {
            return NumeroLinea;
        }

        public int ObtenerPosicionInicial()
        {
            return PosicionInicial;
        }

        public int ObtenerPosicionFinal()
        {
            return PosicionFinal;
        }

        public TipoComponente ObtenerTipo()
        {
            return Tipo;
        }

        public override string ToString()
        {
            StringBuilder concatenar = new StringBuilder();
            concatenar.Append("Tipo de componente: ").Append(ObtenerTipo().ToString()).Append("\n");
            concatenar.Append("Categoria: ").Append(ObtenerCategoria()).Append("\n");
            concatenar.Append("Lexema ").Append(ObtenerLexema()).Append("\n");
            concatenar.Append("Numero de linea ").Append(ObtenerNumeroLinea()).Append("\n");
            concatenar.Append("Posicion inicial ").Append(ObtenerPosicionInicial()).Append("\n");
            concatenar.Append("Posicion Final ").Append(ObtenerPosicionFinal()).Append("\n");

            return concatenar.ToString();
        }
    }
}

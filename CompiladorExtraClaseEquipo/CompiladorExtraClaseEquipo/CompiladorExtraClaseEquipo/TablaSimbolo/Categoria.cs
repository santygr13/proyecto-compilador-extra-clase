﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CompiladorExtraClaseEquipo.TablaSimbolo
{
    public enum Categoria
    {
        IDENTIFICADOR, NUMERO_ENTERO, NUMERO_DECIMAL, FIN_ARCHIVO, IGUAL_QUE, DIFERENTE_QUE, MENOR_IGUAL_QUE, MAYOR_IGUAL_QUE,
        MENOR_QUE, MAYOR_QUE, FROM, SELECT, ORDER, BY, ASC, DESC, OR, AND, WHERE, CAMPO, TABLA ,LITERAL, PALABRA_RESERVADA,INDICE,CONCATENADOR
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CompiladorExtraClaseEquipo.TablaSimbolo
{
    public class TablaDeSimbolos
    {
        private static Dictionary<string, List<ComponenteLexico>> Simbolos = new Dictionary<string, List<ComponenteLexico>>();

        public static void Agregar(ComponenteLexico Componente)
        {
            if (Componente != null && TipoComponente.SIMBOLO.Equals(Componente.ObtenerTipo()))
            {
                ObtenerSimbolo(Componente.ObtenerLexema()).Add(Componente);
            }
        }

        public static List<ComponenteLexico> ObtenerSimbolo(String Lexema)
        {
            if (!Simbolos.ContainsKey(Lexema))
            {
                Simbolos.Add(Lexema, new List<ComponenteLexico>());
            }

            return Simbolos[Lexema];
        }

        public static List<ComponenteLexico> ObtenerTodosSimbolos()
        {
            return Simbolos.Values.SelectMany(Componente => Componente).ToList();
        }

        public static void Limpiar()
        {
            Simbolos.Clear();
        }
    }
}

﻿using CompiladorExtraClaseEquipo.Cachee;
using CompiladorExtraClaseEquipo.ManejadorDeErrores;
using CompiladorExtraClaseEquipo.TablaSimbolo;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace CompiladorExtraClaseEquipo.AnalisisLexico
{
    public class AnalizadorLexico
    {
        private int NumeroLineaActual = 1;
        private int Puntero;
        private String CaracterActual;
        private Linea LineaActual;
        private String Lexema;

        public void EstablecerLineaActual(Linea LineaActual)
        {
            this.LineaActual = LineaActual;
        }

        private void CargarNuevaLinea()
        {
            NumeroLineaActual = NumeroLineaActual + 1;
            LineaActual = Cache.ObtenerLinea(NumeroLineaActual);

            if (LineaActual.ObtenerContenido().Equals("@EOF@"))
            {
                NumeroLineaActual = LineaActual.ObtenerNumero();
            }
            Lexema = "";
            ResetearPuntero();
        }

        private void ResetearPuntero()
        {
            Puntero = 0;
        }

        private void AvanzarPuntero()
        {
            Puntero = Puntero + 1;
        }

        private void DevolverPuntero()
        {
            Puntero = Puntero - 1;
        }
        private void LeerCaracterAntrior()
        {
            DevolverPuntero();
            CaracterActual = LineaActual.ObtenerContenido().Substring(Puntero, 1);
        }

        private void LeerSiguienteCaracter()
        {
            if (LineaActual.ObtenerContenido().Equals("@EOF@"))
            {
                CaracterActual = LineaActual.ObtenerContenido();
            }
            else if (Puntero > LineaActual.ObtenerContenido().Length)
            {
                CaracterActual = "@FL@";
            }
            else
            {

                CaracterActual = LineaActual.ObtenerContenido().Substring(Puntero, 1);
                AvanzarPuntero();
            }
        }

        private void DevorarEspacios()
        {
            while (CaracterActual.Equals(" "))
            {
                LeerSiguienteCaracter();
            }
        }

        private Boolean CaracterActualEsLetra()
        {
            return Char.IsLetter(CaracterActual, 0);
        }

        private Boolean CaracterActualEsDigito()
        {
            return Char.IsDigit(CaracterActual, 0);
        }

        private Boolean CaracterActualEsLetraODigito()
        {
            return Char.IsLetterOrDigit(CaracterActual, 0);
        }

        private Boolean CaracterActualEsGuionBajo()
        {
            return "_".Equals(CaracterActual);
        }

        private Boolean CaracterActualEsEspacioEnBlanco()
        {
            return " ".Equals(CaracterActual);
        }

        private Boolean CaracterActualEsFinDeArchivo()
        {
            if ("\n".Equals(CaracterActual) && NumeroLineaActual == Cache.ObtenerLineas().Count)
            {
                return "\n".Equals(CaracterActual);
            }
            return false;

        }

        private Boolean CaracterActualEsIgual()
        {
            return "=".Equals(CaracterActual);
        }

        private Boolean CaracterActualEsMenor()
        {
            return "<".Equals(CaracterActual);
        }

        private Boolean CaracterActualEsMayor()
        {
            return ">".Equals(CaracterActual);
        }

        private Boolean CaracterActualEsExclamacion()
        {
            return "!".Equals(CaracterActual);
        }

        private Boolean CaracterActualEsComa()
        {
            return ",".Equals(CaracterActual);
        }
        private Boolean CaracterActualEsFinDeLinea()
        {
            return "\r".Equals(CaracterActual);
        }
        
        private Boolean CaracterActualEsPunto()
        {
            return ".".Equals(CaracterActual);
        }
        private void Concatenar()
        {
            Lexema = Lexema + CaracterActual;
        }

        private Boolean ComillaSimple()
        {
            return "'".Equals(CaracterActual);
        }

        public ComponenteLexico FormarComponente()
        {
            ComponenteLexico ComponenteLexico = null;
            int EstadoActual = 0;
            bool ContinuarAnalisis = true;
            Lexema = "";



            while (ContinuarAnalisis)
            {
                if (EstadoActual == 0)
                {
                    LeerSiguienteCaracter();
                    DevorarEspacios();
                    if ("C".Equals(CaracterActual) || "c".Equals(CaracterActual))
                    {
                        EstadoActual = 1;
                        Concatenar();
                    }
                    else if ("T".Equals(CaracterActual) || "t".Equals(CaracterActual))
                    {
                        EstadoActual = 7;
                        Concatenar();

                    }
                    else if (CaracterActualEsMenor())
                    {
                        EstadoActual = 24;
                        Concatenar();
                    }
                    else if (CaracterActualEsMayor())
                    {
                        EstadoActual = 28;
                        Concatenar();
                    }
                    else if (CaracterActualEsIgual())
                    {
                        EstadoActual = 31;
                        Concatenar();
                    }
                    else if (CaracterActualEsExclamacion())
                    {
                        EstadoActual = 32;
                        Concatenar();
                    }
                    else if (CaracterActualEsDigito())
                    {
                        EstadoActual = 14;
                        Concatenar();
                    }
                    else if (ComillaSimple())
                    {
                        EstadoActual = 20;
                        Concatenar();
                    }
                    else if ("S".Equals(CaracterActual) || "s".Equals(CaracterActual))
                    {
                        EstadoActual = 36;
                        Concatenar();

                    }
                    else if ("F".Equals(CaracterActual) || "f".Equals(CaracterActual))
                    {
                        EstadoActual = 48;
                        Concatenar();

                    }
                    else if ("W".Equals(CaracterActual) || "w".Equals(CaracterActual))
                    {
                        EstadoActual = 43;
                        Concatenar();

                    }
                    else if ("O".Equals(CaracterActual) || "o".Equals(CaracterActual))
                    {
                        EstadoActual = 52;
                        Concatenar();

                    }
                    else if ("B".Equals(CaracterActual) || "b".Equals(CaracterActual))
                    {
                        EstadoActual = 58;
                        Concatenar();

                    }
                    else if (CaracterActualEsFinDeArchivo())
                    {
                        EstadoActual = 61;
                        Concatenar();
                    }else if (CaracterActualEsFinDeLinea())
                    {
                        EstadoActual = 37;
                        //Concatenar();
                    }else if("A".Equals(CaracterActual) || "a".Equals(CaracterActual))
                    {
                        EstadoActual = 63;
                        Concatenar();
                    }
                    else if ("D".Equals(CaracterActual) || "d".Equals(CaracterActual))
                    {
                        EstadoActual = 64;
                        Concatenar();
                    }else if (CaracterActualEsComa())
                    {
                        EstadoActual = 72;
                        Concatenar();
                    }

                }
                else if (EstadoActual == 1)
                {
                    LeerSiguienteCaracter();
                    if ("a".Equals(CaracterActual) || "A".Equals(CaracterActual))
                    {
                        EstadoActual = 2;
                        Concatenar();
                    }
                    else
                    {
                        EstadoActual = 5;
                    }
                }
                else if (EstadoActual == 2)
                {
                    LeerSiguienteCaracter();
                    if ("m".Equals(CaracterActual) || "M".Equals(CaracterActual))
                    {
                        EstadoActual = 3;
                        Concatenar();
                    }
                    else
                    {
                        EstadoActual = 5;
                    }
                }
                else if (EstadoActual == 3)
                {
                    LeerSiguienteCaracter();
                    if (CaracterActualEsGuionBajo())
                    {
                        EstadoActual = 4;
                        Concatenar();
                        LeerSiguienteCaracter();
                        if(" ".Equals(CaracterActual) || "\n".Equals(CaracterActual))
                        {
                            EstadoActual = 5;
                        }
                        else
                        {
                            LeerCaracterAntrior();
                        }
                    }
                    else
                    {
                        EstadoActual = 5;
                    }
                }
                else if (EstadoActual == 4)
                {
                    LeerSiguienteCaracter();
                    if (CaracterActualEsGuionBajo() || CaracterActualEsLetraODigito()) 
                    {
                        EstadoActual = 4;
                        Concatenar();
                    }
                    else
                    {
                        EstadoActual = 6;
                    }
                }
                else if (EstadoActual == 5)
                {
                    int temporal = Puntero - 1;
                    while (Char.IsLetter(LineaActual.ObtenerContenido().Substring(temporal, 1), 0))
                    {
                        Concatenar();
                        LeerSiguienteCaracter();
                        temporal++;
                    }

                    Puntero -= 1;
                    //DevolverPuntero();
                    Error error = Error.CrearErrorLexico(Lexema, NumeroLineaActual,
                        Puntero - Lexema.Length + 1, Puntero,
                    "Campo no calido",
                    "Leí \"" + CaracterActual + "\"",
                    "Asegurese de que el campo tenga la sguiente estructura {CAM_<NOMBRE>}");

                    GestorErrores.Reportar(error);

                    ComponenteLexico = ComponenteLexico.CrearDummy(Categoria.CAMPO, Lexema + " La sintaxix correcta es CAM_example", NumeroLineaActual,
                        Puntero - Lexema.Length, Puntero - 1);

                    TablaMaestra.Agregar(ComponenteLexico);

                    MessageBox.Show("EL campo no esta bien formado. La manera correcta es {CAM_<nombre>}");

                    EstadoActual = 0;
                }
                else if (EstadoActual == 6)
                {
                    DevolverPuntero();
                    ComponenteLexico = ComponenteLexico.CrearpalabraReservada(Categoria.CAMPO, Lexema, NumeroLineaActual,
                        Puntero - Lexema.Length + 1, Puntero);
                    TablaMaestra.Agregar(ComponenteLexico);
                    ContinuarAnalisis = false;
                }
                else if (EstadoActual == 7)
                {
                    LeerSiguienteCaracter();
                    if ("a".Equals(CaracterActual) || "A".Equals(CaracterActual))
                    {
                        EstadoActual = 8;
                        Concatenar();
                    }
                    else
                    {
                        EstadoActual = 12;
                    }
                }
                else if (EstadoActual == 8)
                {
                    LeerSiguienteCaracter();
                    if ("b".Equals(CaracterActual) || "B".Equals(CaracterActual))
                    {
                        EstadoActual = 9;
                        Concatenar();
                    }
                    else
                    {
                        EstadoActual = 12;
                    }
                }
                else if (EstadoActual == 9)
                {
                    LeerSiguienteCaracter();
                    if (CaracterActualEsGuionBajo())
                    {
                        EstadoActual = 10;
                        Concatenar();
                    }
                    else
                    {
                        EstadoActual = 12;
                    }
                }
                else if (EstadoActual == 10)
                {
                    LeerSiguienteCaracter();
                    if (CaracterActualEsGuionBajo() || CaracterActualEsLetraODigito())
                    {
                        EstadoActual = 10;
                        Concatenar();
                    }
                    else
                    {
                        EstadoActual = 13;
                    }
                }
                else if (EstadoActual == 11)
                {
                    Lexema = "@EOF@";
                    ComponenteLexico = ComponenteLexico.CrearSimbolo(Categoria.FIN_ARCHIVO, Lexema, NumeroLineaActual,
                        Puntero, Puntero);
                    TablaMaestra.Agregar(ComponenteLexico);
                    ContinuarAnalisis = false;
                }
                else if (EstadoActual == 12)
                {
                    int temporal = Puntero - 1;
                    while (Char.IsLetter(LineaActual.ObtenerContenido().Substring(temporal, 1), 0))
                    {
                        Concatenar();
                        LeerSiguienteCaracter();
                        temporal++;
                    }

                    Puntero -= 1;
                    //DevolverPuntero();
                    Error error = Error.CrearErrorLexico(Lexema, NumeroLineaActual,
                        Puntero - Lexema.Length + 1, Puntero,
                    "Tabla no calido",
                    "Leí \"" + CaracterActual + "\"",
                    "Asegurese de que la tabla tenga la sguiente estructura {TAB_<NOMBRE>}");

                    GestorErrores.Reportar(error);

                    ComponenteLexico = ComponenteLexico.CrearDummy(Categoria.TABLA, Lexema + " La sintaxix correcta es TAB_example", NumeroLineaActual,
                        Puntero - Lexema.Length, Puntero - 1);

                    TablaMaestra.Agregar(ComponenteLexico);

                    EstadoActual = 0;
                }
                else if (EstadoActual == 13)
                {
                    DevolverPuntero();
                    ComponenteLexico = ComponenteLexico.CrearpalabraReservada(Categoria.TABLA, Lexema, NumeroLineaActual,
                        Puntero - Lexema.Length + 1, Puntero);
                    TablaMaestra.Agregar(ComponenteLexico);
                    ContinuarAnalisis = false;
                }
                else if (EstadoActual == 14)
                {
                    LeerSiguienteCaracter();
                    if (CaracterActualEsDigito())
                    {
                        EstadoActual = 14;
                        Concatenar();
                    }
                    else if (CaracterActualEsPunto())
                    {
                        EstadoActual = 15;
                        Concatenar();
                    }
                    else
                    {
                        EstadoActual = 16;
                    }
                }
                else if (EstadoActual == 15)
                {
                    LeerSiguienteCaracter();
                    if (CaracterActualEsDigito())
                    {
                        EstadoActual = 17;
                        Concatenar();
                    }
                    else
                    {
                        EstadoActual = 18;
                    }
                }
                else if (EstadoActual == 16)
                {
                    DevolverPuntero();
                    ComponenteLexico = ComponenteLexico.CrearSimbolo(Categoria.NUMERO_ENTERO, Lexema, NumeroLineaActual,
                        Puntero - Lexema.Length + 1, Puntero);
                    TablaMaestra.Agregar(ComponenteLexico);
                    ContinuarAnalisis = false;
                }
                else if (EstadoActual == 17)
                {
                    LeerSiguienteCaracter();
                    if (CaracterActualEsDigito())
                    {
                        EstadoActual = 17;
                        Concatenar();
                    }
                    else
                    {
                        EstadoActual = 19;
                    }
                }
                else if (EstadoActual == 18)
                {
                    int temporal = Puntero - 1;
                    while (Char.IsLetter(LineaActual.ObtenerContenido().Substring(temporal, 1), 0))
                    {
                        Concatenar();
                        LeerSiguienteCaracter();
                        temporal++;
                    }

                    Puntero -= 1;
                    //DevolverPuntero();
                    Error error = Error.CrearErrorLexico(Lexema, NumeroLineaActual,
                        Puntero - Lexema.Length + 1, Puntero,
                    "Número decimal no válido",
                    "Después del separador decimal leí \"" + CaracterActual + "\"",
                    "Asegurese de que luego del separador decimal se encuentre un digito del 0 al 9");

                    GestorErrores.Reportar(error);

                    ComponenteLexico = ComponenteLexico.CrearDummy(Categoria.NUMERO_DECIMAL, Lexema + "0", NumeroLineaActual,
                        Puntero - Lexema.Length, Puntero - 1);

                    TablaMaestra.Agregar(ComponenteLexico);


                    EstadoActual = 0;
                }
                else if (EstadoActual == 19)
                {
                    DevolverPuntero();
                    ComponenteLexico = ComponenteLexico.CrearSimbolo(Categoria.NUMERO_DECIMAL, Lexema, NumeroLineaActual,
                        Puntero - Lexema.Length + 1, Puntero);
                    TablaMaestra.Agregar(ComponenteLexico);
                    ContinuarAnalisis = false;
                }
                else if (EstadoActual == 20)
                {
                    LeerSiguienteCaracter();
                    if (!ComillaSimple())
                    {
                        EstadoActual = 21;
                        Concatenar();
                    }
                    else
                    {
                        EstadoActual = 60;
                    }

                }
                else if (EstadoActual == 21)
                {
                    LeerSiguienteCaracter();
                    if (!ComillaSimple())
                    {
                        EstadoActual = 21;
                        Concatenar();
                    }
                    else if (ComillaSimple())
                    {
                        EstadoActual = 22;
                        Concatenar();
                    }
                    else
                    {
                        EstadoActual = 60;
                    }
                }
                else if (EstadoActual == 22)
                {
                   // DevolverPuntero();
                    ComponenteLexico = ComponenteLexico.CrearSimbolo(Categoria.LITERAL, Lexema, NumeroLineaActual,
                        Puntero - Lexema.Length + 1, Puntero);
                    TablaMaestra.Agregar(ComponenteLexico);
                    ContinuarAnalisis = false;
                }
                else if (EstadoActual == 23)
                {
                    int temporal = Puntero - 1;
                    while (Char.IsLetter(LineaActual.ObtenerContenido().Substring(temporal, 1), 0))
                    {
                        Concatenar();
                        LeerSiguienteCaracter();
                        temporal++;
                    }

                    Puntero -= 1;
                    //DevolverPuntero();
                    Error error = Error.CrearErrorLexico(Lexema, NumeroLineaActual,
                        Puntero - Lexema.Length + 1, Puntero,
                    "Cadena invalida",
                    " No se digito una palabra reservada válida",
                    "Las palabras soportadas por el lenguaje son : SELECT, FROM, WHERE,ORDER BY");

                    MessageBox.Show("Digitaste caracteres o palabras no soportadas por el lenguaje");

                    GestorErrores.Reportar(error);

                    ComponenteLexico = ComponenteLexico.CrearDummy(Categoria.PALABRA_RESERVADA, Lexema +" {ORDER BY}", NumeroLineaActual,
                        Puntero - Lexema.Length, Puntero - 1);

                    TablaMaestra.Agregar(ComponenteLexico);


                    EstadoActual = 0;

                }
                else if (EstadoActual == 24)
                {
                    LeerSiguienteCaracter();
                    if (CaracterActualEsIgual())
                    {
                        EstadoActual = 25;
                        Concatenar();
                    }
                    else if (CaracterActualEsMayor())
                    {
                        EstadoActual = 26;
                        Concatenar();
                    }
                    else
                    {
                        EstadoActual = 27;
                    }
                }
                else if (EstadoActual == 25)
                {
                    ComponenteLexico = ComponenteLexico.CrearSimbolo(Categoria.MENOR_IGUAL_QUE, Lexema, NumeroLineaActual,
                        Puntero - Lexema.Length + 1, Puntero);
                    TablaMaestra.Agregar(ComponenteLexico);
                    ContinuarAnalisis = false;
                }
                else if (EstadoActual == 26)
                {
                    ComponenteLexico = ComponenteLexico.CrearSimbolo(Categoria.DIFERENTE_QUE, Lexema, NumeroLineaActual,
                        Puntero - Lexema.Length + 1, Puntero);
                    TablaMaestra.Agregar(ComponenteLexico);
                    ContinuarAnalisis = false;
                }
                else if (EstadoActual == 27)
                {
                    DevolverPuntero();
                    ComponenteLexico = ComponenteLexico.CrearSimbolo(Categoria.MENOR_QUE, Lexema, NumeroLineaActual,
                        Puntero - Lexema.Length + 1, Puntero);
                    TablaMaestra.Agregar(ComponenteLexico);
                    ContinuarAnalisis = false;
                }
                else if (EstadoActual == 28)
                {
                    LeerSiguienteCaracter();
                    if (CaracterActualEsIgual())
                    {
                        EstadoActual = 29;
                        Concatenar();
                    }
                    else
                    {
                        EstadoActual = 30;
                    }
                }
                else if (EstadoActual == 29)
                {
                    ComponenteLexico = ComponenteLexico.CrearSimbolo(Categoria.MAYOR_IGUAL_QUE, Lexema, NumeroLineaActual,
                        Puntero - Lexema.Length + 1, Puntero);
                    TablaMaestra.Agregar(ComponenteLexico);
                    ContinuarAnalisis = false;
                }
                else if (EstadoActual == 30)
                {
                    DevolverPuntero();
                    ComponenteLexico = ComponenteLexico.CrearSimbolo(Categoria.MAYOR_QUE, Lexema, NumeroLineaActual,
                        Puntero - Lexema.Length + 1, Puntero);
                    TablaMaestra.Agregar(ComponenteLexico);
                    ContinuarAnalisis = false;
                }
                else if (EstadoActual == 31)
                {
                    ComponenteLexico = ComponenteLexico.CrearSimbolo(Categoria.IGUAL_QUE, Lexema, NumeroLineaActual,
                        Puntero - Lexema.Length + 1, Puntero);
                    TablaMaestra.Agregar(ComponenteLexico);
                    ContinuarAnalisis = false;
                }
                else if (EstadoActual == 32)
                {
                    LeerSiguienteCaracter();
                    if (CaracterActualEsIgual())
                    {
                        EstadoActual = 34;
                        Concatenar();
                    }
                    else
                    {
                        EstadoActual = 33;
                    }
                }
                else if (EstadoActual == 33)
                {
                    Concatenar();
                    Error error = Error.CrearErrorLexico(Lexema, NumeroLineaActual,
                        Puntero - Lexema.Length + 1, Puntero,
                    "Diferente qué no válido",
                    "Después de admiración(!) leí \"" + CaracterActual + "\"",
                    "Asegurese de que luego del signo de admiración(!) se encuentre un igual (=)");


                    GestorErrores.Reportar(error);

                    ComponenteLexico = ComponenteLexico.CrearDummy(Categoria.DIFERENTE_QUE, Lexema + " =", NumeroLineaActual,
                        Puntero - Lexema.Length + 1, Puntero);

                    TablaMaestra.Agregar(ComponenteLexico);


                    EstadoActual = 0;
                }
                else if (EstadoActual == 34)
                {
                    ComponenteLexico = ComponenteLexico.CrearSimbolo(Categoria.DIFERENTE_QUE, Lexema, NumeroLineaActual,
                        Puntero - Lexema.Length + 1, Puntero);
                    TablaMaestra.Agregar(ComponenteLexico);
                    ContinuarAnalisis = false;
                }
                else if (EstadoActual == 35)
                {
                    Concatenar();
                    Error error = Error.CrearErrorLexico(Lexema, NumeroLineaActual,
                        Puntero - Lexema.Length + 1, Puntero,
                    "Símbolo no válido",
                    "Leí \"" + CaracterActual + "\"",
                    "Asegúrese de que los simbolos ingresados sean válidos");

                    GestorErrores.Reportar(error);

                    throw new Exception("Se ha presentado un errores Léxico que detiene el proceso" +
                        "Por favor validar la consola de errores y solucionar el problema para realizar" +
                        "nuevamente el proceso de compilación...");
                }
                else if (EstadoActual == 36)
                {
                    LeerSiguienteCaracter();
                    if ("E".Equals(CaracterActual) || "e".Equals(CaracterActual))
                    {
                        EstadoActual = 38;
                        Concatenar();

                    }
                    else
                    {
                        EstadoActual = 23;
                    }
                }
                else if (EstadoActual == 37)
                {
                    
                    EstadoActual = 0;
                    CargarNuevaLinea();
                    
                    
                }
                else if (EstadoActual == 38)
                {
                    LeerSiguienteCaracter();
                    if ("L".Equals(CaracterActual) || "l".Equals(CaracterActual))
                    {
                        EstadoActual = 39;
                        Concatenar();

                    }
                    else
                    {
                        EstadoActual = 23;
                    }
                }
                else if (EstadoActual == 39)
                {
                    LeerSiguienteCaracter();
                    if ("E".Equals(CaracterActual) || "e".Equals(CaracterActual))
                    {
                        EstadoActual = 40;
                        Concatenar();

                    }
                    else
                    {
                        EstadoActual = 23;
                    }
                }
                else if (EstadoActual == 40)
                {
                    LeerSiguienteCaracter();
                    if ("C".Equals(CaracterActual) || "c".Equals(CaracterActual))
                    {
                        EstadoActual = 41;
                        Concatenar();

                    }
                    else
                    {
                        EstadoActual = 23;
                    }
                }
                else if (EstadoActual == 41)
                {
                    LeerSiguienteCaracter();
                    if ("T".Equals(CaracterActual) || "t".Equals(CaracterActual))
                    {
                        EstadoActual = 42;
                        Concatenar();
                        AvanzarPuntero();
                    }
                    else
                    {
                        EstadoActual = 23;
                    }
                }
                else if (EstadoActual == 42)
                {
                    DevolverPuntero();
                    ComponenteLexico = ComponenteLexico.CrearpalabraReservada(Categoria.SELECT, Lexema, NumeroLineaActual,
                        Puntero - Lexema.Length + 1, Puntero);
                    TablaMaestra.Agregar(ComponenteLexico);
                    ContinuarAnalisis = false;

                }
                else if (EstadoActual == 43)
                {
                    LeerSiguienteCaracter();
                    if ("H".Equals(CaracterActual) || "h".Equals(CaracterActual))
                    {
                        EstadoActual = 44;
                        Concatenar();

                    }
                    else
                    {
                        EstadoActual = 23;
                    }
                }
                else if (EstadoActual == 44)
                {
                    LeerSiguienteCaracter();
                    if ("E".Equals(CaracterActual) || "e".Equals(CaracterActual))
                    {
                        EstadoActual = 45;
                        Concatenar();

                    }
                    else
                    {
                        EstadoActual = 23;
                    }
                }
                else if (EstadoActual == 45)
                {
                    LeerSiguienteCaracter();
                    if ("R".Equals(CaracterActual) || "r".Equals(CaracterActual))
                    {
                        EstadoActual = 46;
                        Concatenar();

                    }
                    else
                    {
                        EstadoActual = 23;
                    }
                }
                else if (EstadoActual == 46)
                {
                    LeerSiguienteCaracter();
                    if ("E".Equals(CaracterActual) || "e".Equals(CaracterActual))
                    {
                        EstadoActual = 47;
                        Concatenar();

                    }
                    else
                    {
                        EstadoActual = 23;
                    }
                }
                else if (EstadoActual == 47)
                {
                    DevolverPuntero();
                    ComponenteLexico = ComponenteLexico.CrearpalabraReservada(Categoria.WHERE, Lexema, NumeroLineaActual,
                        Puntero - Lexema.Length + 1, Puntero);
                    TablaMaestra.Agregar(ComponenteLexico);
                    ContinuarAnalisis = false;
                }
                else if (EstadoActual == 48)
                {
                    LeerSiguienteCaracter();
                    if ("R".Equals(CaracterActual) || "r".Equals(CaracterActual))
                    {
                        EstadoActual = 49;
                        Concatenar();

                    }
                    else
                    {
                        EstadoActual = 23;
                    }
                }
                else if (EstadoActual == 49)
                {
                    LeerSiguienteCaracter();
                    if ("O".Equals(CaracterActual) || "o".Equals(CaracterActual))
                    {
                        EstadoActual = 50;
                        Concatenar();

                    }
                    else
                    {
                        EstadoActual = 23;
                    }
                }
                else if (EstadoActual == 50)
                {
                    LeerSiguienteCaracter();
                    if ("M".Equals(CaracterActual) || "m".Equals(CaracterActual))
                    {
                        EstadoActual = 51;
                        Concatenar();

                    }
                    else
                    {
                        EstadoActual = 23;
                    }
                }
                else if (EstadoActual == 51)
                {
                    DevolverPuntero();
                    ComponenteLexico = ComponenteLexico.CrearpalabraReservada(Categoria.FROM, Lexema, NumeroLineaActual,
                        Puntero - Lexema.Length + 1, Puntero);
                    TablaMaestra.Agregar(ComponenteLexico);
                    ContinuarAnalisis = false;
                }
                else if (EstadoActual == 52)
                {
                    LeerSiguienteCaracter();
                    if ("R".Equals(CaracterActual) || "r".Equals(CaracterActual))
                    {
                        EstadoActual = 53;
                        Concatenar();

                    }
                    else
                    {
                        EstadoActual = 23;
                    }
                }
                else if (EstadoActual == 53)
                {
                
                    LeerSiguienteCaracter();
                    if(" ".Equals(CaracterActual))
                    {
                        EstadoActual = 75;
                        Concatenar();
                        AvanzarPuntero();
                    }
                    else if ("D".Equals(CaracterActual) || "d".Equals(CaracterActual))
                    {
                        EstadoActual = 54;
                        Concatenar();

                    }
                    else
                    {
                        EstadoActual = 23;
                    }
                }
                else if (EstadoActual == 54)
                {
                    LeerSiguienteCaracter();
                    if ("E".Equals(CaracterActual) || "e".Equals(CaracterActual))
                    {
                        EstadoActual = 55;
                        Concatenar();

                    }
                    else
                    {
                        EstadoActual = 23;
                    }
                }
                else if (EstadoActual == 55)
                {
                    LeerSiguienteCaracter();
                    if ("R".Equals(CaracterActual) || "r".Equals(CaracterActual))
                    {
                        EstadoActual = 56;
                        Concatenar();

                    }
                    else
                    {
                        EstadoActual = 23;
                    }
                }
                else if (EstadoActual == 56)
                {
                    //DevolverPuntero();
                    ComponenteLexico = ComponenteLexico.CrearpalabraReservada(Categoria.ORDER, Lexema, NumeroLineaActual,
                        Puntero - Lexema.Length + 1, Puntero);
                    TablaMaestra.Agregar(ComponenteLexico);
                    ContinuarAnalisis = false;
                }
                else if (EstadoActual == 57)
                {
                    
                }
                else if (EstadoActual == 58)
                {
                    LeerSiguienteCaracter();
                    if ("Y".Equals(CaracterActual) || "y".Equals(CaracterActual))
                    {
                        EstadoActual = 59;
                        Concatenar();

                    }
                    else
                    {
                        EstadoActual = 23;
                    }
                }
                else if (EstadoActual == 59)
                {
                    DevolverPuntero();
                    ComponenteLexico = ComponenteLexico.CrearpalabraReservada(Categoria.BY, Lexema, NumeroLineaActual,
                        Puntero - Lexema.Length + 1, Puntero);
                    TablaMaestra.Agregar(ComponenteLexico);
                    ContinuarAnalisis = false;
                }else if (EstadoActual == 60)
                {

                    Concatenar();
                    Error error = Error.CrearErrorLexico(Lexema, NumeroLineaActual,
                        Puntero - Lexema.Length + 1, Puntero,
                    "Literal no valido",
                    "Digistaste \"" + CaracterActual + "\"",
                    "La manera correcta de escribirun literal es ('contenido')");


                    GestorErrores.Reportar(error);

                    ComponenteLexico = ComponenteLexico.CrearDummy(Categoria.LITERAL, Lexema + " =", NumeroLineaActual,
                        Puntero - Lexema.Length + 1, Puntero);

                    TablaMaestra.Agregar(ComponenteLexico);


                    EstadoActual = 0;
                }else if (EstadoActual == 61)
                {
                    Lexema = "@EOF@";
                    ComponenteLexico = ComponenteLexico.CrearSimbolo(Categoria.FIN_ARCHIVO, Lexema, NumeroLineaActual,
                        Puntero, Puntero);
                    TablaMaestra.Agregar(ComponenteLexico);
                    ContinuarAnalisis = false;
                }
                else if (EstadoActual == 63)
                {
                    LeerSiguienteCaracter();
                    if ("S".Equals(CaracterActual) || "s".Equals(CaracterActual))
                    {
                        EstadoActual = 65;
                        Concatenar();

                    }else if("N".Equals(CaracterActual) || "n".Equals(CaracterActual))
                    {
                        EstadoActual = 73;
                        Concatenar();
                    }
                    else
                    {
                        EstadoActual = 67;
                    }
                }
                else if (EstadoActual == 64)
                {
                    LeerSiguienteCaracter();
                    if ("E".Equals(CaracterActual) || "e".Equals(CaracterActual))
                    {
                        EstadoActual = 68;
                        Concatenar();

                    }
                    else
                    {
                        EstadoActual = 71;
                    }
                }
                else if (EstadoActual == 65)
                {
                    LeerSiguienteCaracter();
                    if ("C".Equals(CaracterActual) || "c".Equals(CaracterActual))
                    {
                        EstadoActual = 66;
                        Concatenar();
                        AvanzarPuntero();
                    }
                    else
                    {
                        EstadoActual = 67;
                    }
                }
                else if (EstadoActual == 66)
                {
                    DevolverPuntero();
                    ComponenteLexico = ComponenteLexico.CrearpalabraReservada(Categoria.ASC, Lexema, NumeroLineaActual,
                        Puntero - Lexema.Length + 1, Puntero);
                    TablaMaestra.Agregar(ComponenteLexico);
                    ContinuarAnalisis = false;
                }
                else if (EstadoActual == 67)
                {

                    Concatenar();
                    Error error = Error.CrearErrorLexico(Lexema, NumeroLineaActual,
                        Puntero - Lexema.Length + 1, Puntero,
                    "Palabra no valida",
                    "Digistaste \"" + CaracterActual + "\"",
                    "La manera correcta de escribir es ASC o AND");


                    GestorErrores.Reportar(error);

                    ComponenteLexico = ComponenteLexico.CrearDummy(Categoria.ASC, Lexema + " ASC ", NumeroLineaActual,
                        Puntero - Lexema.Length + 1, Puntero);

                    TablaMaestra.Agregar(ComponenteLexico);


                    EstadoActual = 0;
                }
                else if (EstadoActual == 68)
                {
                    LeerSiguienteCaracter();
                    if ("S".Equals(CaracterActual) || "s".Equals(CaracterActual))
                    {
                        EstadoActual = 69;
                        Concatenar();

                    }
                    else
                    {
                        EstadoActual = 71;
                    }
                }
                else if (EstadoActual == 69)
                {
                    LeerSiguienteCaracter();
                    if ("C".Equals(CaracterActual) || "c".Equals(CaracterActual))
                    {
                        EstadoActual = 70;
                        Concatenar();
                        AvanzarPuntero();
                    }
                    else
                    {
                        EstadoActual = 71;
                    }
                }
                else if (EstadoActual == 70)
                {
                    DevolverPuntero();
                    ComponenteLexico = ComponenteLexico.CrearpalabraReservada(Categoria.DESC, Lexema, NumeroLineaActual,
                        Puntero - Lexema.Length + 1, Puntero);
                    TablaMaestra.Agregar(ComponenteLexico);
                    ContinuarAnalisis = false;
                }
                else if (EstadoActual == 71)
                {

                    Concatenar();
                    Error error = Error.CrearErrorLexico(Lexema, NumeroLineaActual,
                        Puntero - Lexema.Length + 1, Puntero,
                    "Palabra no valida",
                    "Digistaste \"" + CaracterActual + "\"",
                    "La manera correcta de escribir es DESC");


                    GestorErrores.Reportar(error);

                    ComponenteLexico = ComponenteLexico.CrearDummy(Categoria.DESC, Lexema + " DESC ", NumeroLineaActual,
                        Puntero - Lexema.Length + 1, Puntero);

                    TablaMaestra.Agregar(ComponenteLexico);


                    EstadoActual = 0;
                }
                else if (EstadoActual == 72)
                {
                    ComponenteLexico = ComponenteLexico.CrearSimbolo(Categoria.CONCATENADOR, Lexema, NumeroLineaActual,
                         Puntero - Lexema.Length + 1, Puntero);
                    TablaMaestra.Agregar(ComponenteLexico);
                    ContinuarAnalisis = false;
                }
                else if (EstadoActual == 73)
                {
                    LeerSiguienteCaracter();
                    if ("D".Equals(CaracterActual) || "d".Equals(CaracterActual))
                    {
                        EstadoActual = 74;
                        Concatenar();
                        AvanzarPuntero();

                    }
                    else
                    {
                        EstadoActual = 67;
                    }
                }
                else if (EstadoActual == 74)
                {
                    DevolverPuntero();
                    ComponenteLexico = ComponenteLexico.CrearpalabraReservada(Categoria.AND, Lexema, NumeroLineaActual,
                        Puntero - Lexema.Length + 1, Puntero);
                    TablaMaestra.Agregar(ComponenteLexico);
                    ContinuarAnalisis = false;
                }else if (EstadoActual == 75)
                {
                    DevolverPuntero();
                    ComponenteLexico = ComponenteLexico.CrearpalabraReservada(Categoria.OR, Lexema, NumeroLineaActual,
                        Puntero - Lexema.Length + 1, Puntero);
                    TablaMaestra.Agregar(ComponenteLexico);
                    ContinuarAnalisis = false;
                }
            }

            return ComponenteLexico;
        }


    }
}
